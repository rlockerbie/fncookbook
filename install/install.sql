
-- --------------------------------------------------------

--
-- Table structure for table `fn_cookbook_recipe`
--

CREATE TABLE IF NOT EXISTS `fn_cookbook_recipe` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`title` varchar(255) NOT NULL,
`description` text NOT NULL,
`notes` text NOT NULL,
`serves` varchar(255) NOT NULL,
`is_deleted` tinyint(1) NOT NULL DEFAULT '0',
`dt_created` datetime NOT NULL,
`dt_modified` datetime NOT NULL,
`modifier_id` bigint(20) NOT NULL,
`creator_id` bigint(20) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;


--
-- Table structure for table `fn_cookbook_method`
--

CREATE TABLE IF NOT EXISTS `fn_cookbook_method` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`recipe_id` bigint(20) NOT NULL,
`method` text NOT NULL,
`is_deleted` tinyint(1) NOT NULL DEFAULT '0',
`dt_created` datetime NOT NULL,
`dt_modified` datetime NOT NULL,
`modifier_id` bigint(20) NOT NULL,
`creator_id` bigint(20) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;


--
-- Table structure for table `fn_cookbook_ingredient`
--

CREATE TABLE IF NOT EXISTS `fn_cookbook_ingredient` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`method_id` bigint(20) NOT NULL,
`title` varchar(255) NOT NULL,
`quantity` varchar(255) NOT NULL,
`unit` varchar(255) NOT NULL,
`is_deleted` tinyint(1) NOT NULL DEFAULT '0',
`dt_created` datetime NOT NULL,
`dt_modified` datetime NOT NULL,
`modifier_id` bigint(20) NOT NULL,
`creator_id` bigint(20) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
