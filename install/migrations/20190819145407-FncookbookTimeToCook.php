<?php

class FncookbookTimeToCook extends CmfiveMigration {

	public function up() {
		// UP
		$this->addColumnToTable('fn_cookbook_recipe', 'time_to_cook', 'string', ['null' => true, 'length' => 255]);
		$this->addColumnToTable('fn_cookbook_method', 'title', 'string', ['null' => true, 'length' => 255]);
	}

	public function down() {
		// DOWN
		$this->removeColumnFromTable('fn_cookbook_recipe', 'time_to_cook');
		$this->removeColumnFromTable('fn_cookbook_method', 'title');
	}
}
