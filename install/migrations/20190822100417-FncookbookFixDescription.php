<?php

class FncookbookFixDescription extends CmfiveMigration {

	public function up() {
		// UP
		$this->renameColumnInTable('fn_cookbook_recipe', 'descrption', 'description');

	}

	public function down() {
		// DOWN
		$this->renameColumnInTable('fn_cookbook_recipe', 'description', 'descrption');
	}
}
