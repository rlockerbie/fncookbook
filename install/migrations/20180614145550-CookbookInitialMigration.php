<?php

class CookbookInitialMigration extends CmfiveMigration {

	public function up() {
		// UP
		$column = parent::Column();
		$column->setName('id')
				->setType('biginteger')
				->setIdentity(true);

		// Recipe table
		if (!$this->hasTable("fn_cookbook_recipe")) {
			$this->table('fn_cookbook_recipe', [
				'id'          => false,
				'primary_key' => 'id'
			])->addColumn($column)
				->addColumn('title', 'string', ['limit' => 255])
				->addColumn('descrption', 'text')
				->addColumn('notes', 'text')
				->addColumn('serves', 'string', ['limit' => 255])
				->addCmfiveParameters()
				->create();
		}
		
		// Method table
		if (!$this->hasTable("fn_cookbook_method")) {
			$this->table('fn_cookbook_method', [
				'id'          => false,
				'primary_key' => 'id'
			])->addColumn($column)
				->addColumn('recipe_id', 'biginteger')
				->addColumn('method', 'text')
				->addCmfiveParameters()
				->create();
		}
		
		// Ingredient table
		if (!$this->hasTable("fn_cookbook_ingredient")) {
			$this->table('fn_cookbook_ingredient', [
				'id'          => false,
				'primary_key' => 'id'
			])->addColumn($column)
				->addColumn('method_id', 'biginteger')
				->addColumn('title', 'string', ['limit' => 255])
				->addColumn('quantity', 'string', ['limit' => 255])
				->addColumn('unit', 'string', ['limit' => 255])
				->addCmfiveParameters()
				->create();
		}
	}

	public function down() {
		// DOWN
		$this->hasTable("fn_cookbook_recipe") ? $this->dropTable('fn_cookbook_recipe') : null;
		$this->hasTable("fn_cookbook_method") ? $this->dropTable('fn_cookbook_method') : null;
		$this->hasTable("fn_cookbook_ingredient") ? $this->dropTable('fn_cookbook_ingredient') : null;
	}

}
