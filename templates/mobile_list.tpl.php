<div class="row">
	<div class="columns large-12">
		<h1>Lockerbie's Cookbook</h1>
	</div>
</div>
<div class="row">
	<?php foreach($recipes as $recipe): ?>
	<div class="columns large-3">
		<h5><a href="/fncookbook/mobile_view/<?php echo $recipe->id; ?>"><?php echo $recipe->title; ?></a></h5>
	</div>
	<?php endforeach; ?>
</div>
