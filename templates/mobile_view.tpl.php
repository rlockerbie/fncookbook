<div class="row">
	<div class="columns large-12">
		<a href="//ncookbook/mobile_view">Back</a>
	</div>
	<div class="columns large-12">
		<h3><?php echo $title; ?></h3>
	</div>
	<?php if(!empty($description)): ?>
	<div class="columns large-12">
		<h4>Description</h4>
		<?php echo $description; ?>
	</div>
	<?php endif; ?>
	<?php if(!empty($notes)): ?>
	<div class="columns large-12">
		<h4>Notes</h4>
		<?php echo $notes; ?>
	</div>
	<?php endif; ?>
</div>
<div class="methods row">
	<?php foreach($methods as $method): ?>
		<div class="recipe_ingredients columns large-3 small-5">
			<h5>Ingredients</h5>
			<ul>
			<?php foreach($method['ingredients'] as $ingredient): ?>
				<li data-id="<?php echo $ingredient['id']; ?>">
				<span class="ingredient_container">
				<?php echo $ingredient['quantity']; ?>
				<?php echo $ingredient['unit']; ?>
				<?php echo $ingredient['title']; ?>
				</span>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<div class="recipe_method_container columns large-9 small-7">
			<div class="recipe_method" data-text="<?php echo $method['method']; ?>">
				<?php echo $method['method_parsed']; ?>
			</div>
		</div>
	<?php endforeach; ?>
</div>