<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo !empty($title) ? $title : 'FN-Cookbook'; ?></title>
	<meta charset=utf-8>
	<meta http-equiv=X-UA-Compatible content="IE=edge">
	<meta name=viewport content="width=device-width,initial-scale=1">
	<link rel=stylesheet href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
	<link rel=stylesheet href=https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css>
	<?php
	$w->outputStyles();
	?>

</head>

<body>
	<div id="app"></div>
	<?php
	echo $body;
	$w->outputScripts();
	?>
</body>

</html>