<div class="row">
	<div class="columns large-12">
		<h4>Description</h4>
		<?php echo $description; ?>
	</div>
	<div class="columns large-12">
		<h4>Notes</h4>
		<?php echo $notes; ?>
	</div>
</div>
<div class="methods row">
	<?php foreach($methods as $method): ?>
		<div class="recipe_ingredients columns large-3 small-5">
			<h5>Ingredients <a href="#" onclick="return addIngredient(<?php echo $method['id']; ?>);">+</a></h5>
			<ul>
			<?php foreach($method['ingredients'] as $ingredient): ?>
				<li data-id="<?php echo $ingredient['id']; ?>">
				<span class="ingredient_container" data-row='<?php echo json_encode($ingredient); ?>' id="ingredient_<?php echo $ingredient['id']; ?>">
				<?php echo $ingredient['quantity']; ?>
				<?php echo $ingredient['unit']; ?>
				<?php echo $ingredient['title']; ?>
				</span>
				<a href="#" class="edit_ingredient" onclick="editIngredient(<?php echo $ingredient['id']; ?>);"><span class="fi-pencil"></span></a>
				<a href="#" class="save_ingredient" style="display:none;" onclick="saveIngredient(<?php echo $ingredient['id']; ?>);"><span class="fi-save"></span></a>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<div class="recipe_method_container columns large-9 small-7" id="recipe_method_<?php echo $method['id']; ?>" data-id="<?php echo $method['id']; ?>">
			<div class="recipe_method" data-text="<?php echo $method['method']; ?>">
				<?php echo $method['method_parsed']; ?>
			</div>
			<p>
				<button class="button tiny" onclick="editMethod(<?php echo $method['id']; ?>);">Edit</button>
				<button class="button tiny save_method_btn" style="display:none:" onclick="saveMethod(<?php echo $method['id']; ?>);">Save</button>
			</p>
		</div>
	<?php endforeach; ?>
	<div class="columns large-12">
		<h4>Add method</h4>
		<textarea rows="5" cols="40" id="method_notes" placeholder="Method notes"></textarea>
		<button onclick="addMethod();">Add</button>
	</div>
</div>
<script>
	var token;
	$(function() {
		$.ajax("/rest/token?apikey=<?php echo Config::get("system.rest_api_key") ?>",
			{cache: false,dataType: "json"}
		).done(function(t) {
			token = t.success;
		});
	});
	function saveIngredient(id) {
		var el = $('#ingredient_' + id);
		$.ajax(
			"/rest/save/CookbookIngredient/?token=" + token,
			{cache: false,dataType: "json",method:"POST", data:{
					'id': id,
					'title': el.find('.ingredient_title').val(),
					'quantity': el.find('.ingredient_quantity').val(),
					'unit': el.find('.ingredient_unit').val(),
			}}
		).done(function(response) {
			el.parent().find('.save_ingredient').hide();
			el.parent().find('.edit_ingredient').show();
			el.html(
				el.find('.ingredient_unit').val() + ' ' +
				el.find('.ingredient_title').val() + '  ' +
				el.find('.ingredient_quantity').val()
			);
		});
	}
	function editIngredient(id) {
		var row = $('#ingredient_' + id).data('row');
		$('#ingredient_' + id).parent().find('.save_ingredient').show();
		$('#ingredient_' + id).parent().find('.edit_ingredient').hide();
		$('#ingredient_' + id).html(
			'<input type="text" placeholder="Ingredient" class="ingredient_title" value="' + row['title'] + '">' +
			'<input type="text" placeholder="Quantity" class="ingredient_quantity" value="' + row['quantity'] + '">' +
			'<input type="text" placeholder="Unit" class="ingredient_unit" value="' + row['unit'] + '">'
		);
	}
	function addIngredient(id) {
		console.log('Adding ingredient for method with ID ' + id);
		$.ajax(
			"/rest/save/CookbookIngredient/?token=" + token,
			{
					cache: false,
					dataType: "json",
					method:"POST",
					data: {
						'method_id': id,
						'title': '[placeholder]',
						'unit': '[cups]',
						'quantity': '[1]'
					}
			}
		).done(function(response) {
			$('.recipe_ingredients ul').append('<li data-id="' + response.success.id + '">' +
				'<span class="ingredient_container" data-row=\'' + JSON.stringify(response.success) + '\' id="ingredient_' + response.success.id + '">' +
				response.success.title + ' x' +
				response.success.quantity +
				response.success.unit +
				'</span>' +
				'<a href="#" class="edit_ingredient" onclick="editIngredient(' + response.success.id + ');"><span class="fi-pencil"></span></a>' +
				'<a href="#" class="save_ingredient" style="display:none;" onclick="saveIngredient(' + response.success.id + ');"><span class="fi-save"></span></a>' +
				'</li>');
		});
	}
	function editMethod(id) {
		var text = $('#recipe_method_' + id + ' .recipe_method').data('text');
		$('#recipe_method_' + id + ' .recipe_method .save_method_btn').show();
		$('#recipe_method_' + id + ' .recipe_method').html('<textarea>' + text + '</textarea>');
	}
	function saveMethod(id) {
		$(this).hide();
		var text = $('#recipe_method_' + id + ' .recipe_method textarea').val();
		$.ajax(
			"/rest/save/CookbookMethod/?token=" + token,
			{cache: false,dataType: "json",method:"POST", data:{'id': id, 'method': text}}
		).done(function(response) {
			$.ajax(
				"/fn_cookbook",
				{cache: false,dataType: "json",method:"POST", data:{'parse_markdown': text}}
			).done(function(response) {
				$('#recipe_method_' + id + ' .recipe_method').html(response.parsed_html);
				$('#recipe_method_' + id + ' .recipe_method').data('text', text);
			});
		});
	}
	function addMethod() {
		$.ajax(
			"/rest/save/CookbookMethod/?token=" + token,
			{cache: false,dataType: "json",method:"POST", data:{'recipe_id': '<?php echo $id ?>','method': $('#method_notes').val()}}
		).done(function(response) {
			if (response.success > 0) {
				// we got a result containing the updated record
				console.log(response.success);
				document.location.reload();
			}
		});
	}
</script>