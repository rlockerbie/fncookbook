<?php

function load_recipes_GET(Web $w)
{
    $w->setLayout(null);
    header('Content-type: application/json');

    $tags = Request::string('tags');
    $term = Request::string('term');

    if (empty($tags)) {
        $recipes = FnCookbookService::getInstance($w)->getRecipes($term);
    } else {
        $recipes = FnCookbookService::getInstance($w)->getRecipesByTags($term, explode(',', $tags));
    }
    
    $data = [];

    if (!empty($recipes)) {
        foreach($recipes as $recipe) {
            $data[$recipe->id] = [
                'title' => $recipe->title,
                'description' => $recipe->description,
                'id' => $recipe->id,
                'time_to_cook' => $recipe->time_to_cook,
            ];
        }
    }

    $w->out(json_encode($data));
}

function get_all_tags_GET(Web $w)
{
    $w->setLayout(null);
    header('Content-type: application/json');
    
    $data = [];
    $tags = TagService::getInstance($w)->getTagsByObjectClass('FnCookbookRecipe');
    $uniqueTags = [];
    if (!empty($tags)) {
        foreach ($tags as $tag) {
            if (in_array($tag->id, $uniqueTags)) {
                continue;
            }
            $uniqueTags[] = $tag->id;
            $tTag = $tag->toArray();
            $tTag['enabled'] = false;
            $data[] = $tTag;
        }
    }

    $w->out(json_encode($data));
}

function get_tags_GET(Web $w)
{
    $w->setLayout(null);
    header('Content-type: application/json');

    list($id) = $w->pathMatch('id');
    $recipe = FnCookbookService::getInstance($w)->getRecipe($id);

	if (empty($recipe->id)) {
		return;
    }
    
    $data = [];
    $tags = TagService::getInstance($w)->getTagsByObject($recipe);
    if (!empty($tags)) {
        foreach ($tags as $tag) {
            $data[] = $tag->toArray();
        }
    }

    $w->out(json_encode($data));
}

function load_recipe_GET(Web $w)
{
    $w->setLayout(null);
    header('Content-type: application/json');
    list($id) = $w->pathMatch('id');
    $recipe = FnCookbookService::getInstance($w)->getRecipe($id);
    
    $data = [];

    if (!empty($recipe)) {
        $data = [
            'title' => $recipe->title,
            'text' => $recipe->description,
            'id' => $recipe->id,
            'methods' => []
        ];
        $methods = FnCookbookService::getInstance($w)->loadMethodsForRecipe($id);
        if (!empty($methods)) {
            foreach ($methods as $method) {
                $data['methods'][] = $method;
            }
        }
    }
    $w->out(json_encode($data));
}

function load_recipe_for_edit_GET(Web $w)
{
    $w->setLayout(null);
    header('Content-type: application/json');
    list($id) = $w->pathMatch('id');
    $recipe = FnCookbookService::getInstance($w)->getRecipe($id);
    
    $data = [];

    if (!empty($recipe)) {
        $data = $recipe->toArray();
        $data['methods'] = [];
        $methods = FnCookbookService::getInstance($w)->loadMethodsForRecipe($id);
        if (!empty($methods)) {
            foreach ($methods as $method) {
                $data['methods'][] = $method;
            }
        }
    }
    $w->out(json_encode($data));
}

function save_recipe_POST(Web $w)
{
    $w->setLayout(null);

    $postdata = file_get_contents("php://input");
    header('Content-type: application/json');
    $request = json_decode($postdata);

    if (!empty($request)) {
        if (!empty($request->id)) {
            $recipe = FnCookbookService::getInstance($w)->getRecipe($request->id);
        } else {
            $recipe = new FnCookbookRecipe($w);
        }
    }
    if (empty($recipe)) {
        $w->out(json_encode(['id' => 0, 'error' => true]));
        return;
    }

    $recipe->title = empty($request->title) ? '' : $request->title;
    $recipe->description = empty($request->description) ? '' : $request->description;
    $recipe->notes = empty($request->notes) ? '' : $request->notes;
    $recipe->serves = empty($request->serves) ? '' : $request->serves;
    $recipe->time_to_cook = empty($request->time_to_cook) ? '' : $request->time_to_cook;

    $recipe->insertOrUpdate();
    $recipeId = $recipe->id;

    if (!empty($request->methods)) {
        foreach ($request->methods as $methodData) {
            if (!empty($methodData->id)) {
                $method = FnCookbookService::getInstance($w)->getMethod($methodData->id);
            } else {
                $method = new FnCookbookMethod($w);
            }
            $method->recipe_id = $recipeId;
            $method->title = empty($methodData->title) ? '' : $methodData->title;
            $method->method = empty($methodData->method) ? '' : filter_var($methodData->method, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $method->insertOrUpdate();
            $methodId = $method->id;
            if (!empty($methodData->ingredients)) {
                foreach ($methodData->ingredients as $ingredientData) {
                    if (!empty($ingredientData->id)) {
                        $ingredient = FnCookbookService::getInstance($w)->getIngredient($ingredientData->id);
                    } else {
                        $ingredient = new FnCookbookIngredient($w);
                    }
                    $ingredient->title = empty($ingredientData->title) ? '' : $ingredientData->title;
                    $ingredient->quantity = empty($ingredientData->quantity) ? '' : $ingredientData->quantity;
                    $ingredient->unit = empty($ingredientData->unit) ? '' : $ingredientData->unit;
                    $ingredient->method_id = $methodId;
                    $ingredient->insertOrUpdate();
                }
            }
        }
    }
    $w->out(json_encode(['id' => $recipeId, 'error' => false]));
    return;
}

function remove_tag_POST(Web $w)
{
    $postdata = file_get_contents("php://input");
    header('Content-type: application/json');

    list($id, $tag_id) = $w->pathMatch();
    $class = 'FnCookbookRecipe';

    $object_target = TagService::getInstance($w)->getObject($class, $id);
	if (empty($object_target->id)) {
		return;
	}
	
	$tag = TagService::getInstance($w)->getTag($tag_id);
	if (empty($tag->id)) {
		return;
	}
	
	// Check that tag actually assigned
	$existing_tag_assign = TagService::getInstance($w)->getObject('TagAssign', ['object_class' => $class, 'object_id' => $id, 'tag_id' => $tag->id, 'is_deleted' => 0]);
	if (!empty($existing_tag_assign)) {
		$existing_tag_assign->delete();
	}
	
	$w->out('{}');
}

function add_tag_POST(Web $w)
{
    $w->setLayout(null);
    
    $postdata = file_get_contents("php://input");
    header('Content-type: application/json');
    $request = json_decode($postdata);

    list($id) = $w->pathMatch();
    $class = 'FnCookbookRecipe';
	$new_tag = $request->tag;
	
	if (empty($class) || empty($id) || empty($new_tag)) {
		return;
	}
	
	if (!class_exists($class)) {
		return;
	}
	
	// Check if tag exists
	$tag = TagService::getInstance($w)->getObject("Tag", ['tag' => trim(strip_tags($new_tag)), 'is_deleted' => 0]);

	if (empty($tag->id)) {
		$tag = new Tag($w);
		$tag->tag = $new_tag;
		$tag->insert();
	}

	// Check that tag is not already assigned
	$tag_assign = TagService::getInstance($w)->getObject('TagAssign', ['tag_id' => $tag->id, 'object_class' => $class, 'object_id' => $id, 'is_deleted' => 0]);
	
	if (empty($tag_assign->id)) {
		// Assign object to tag
		$tag_assign = new TagAssign($w);
		$tag_assign->tag_id = $tag->id;
		$tag_assign->object_class = $class;
		$tag_assign->object_id = $id;
		$tag_assign->insert();
	}

	$w->out(json_encode(['id' => $tag->id, 'tag' => $tag->tag], JSON_FORCE_OBJECT));
}

function save_ingredient_POST(Web $w)
{
    $w->setLayout(null);

    $postdata = file_get_contents("php://input");
    header('Content-type: application/json');
    $request = json_decode($postdata);
    if (!empty($request->id)) {
        $ingredient = FnCookbookService::getInstance($w)->getIngredient($request->id);
        if (!empty($ingredient)) {
            $ingredient->is_deleted = $request->is_deleted;
            $ingredient->update();
        }
    }
    $w->out(json_encode(['error' => false]));
    return;
}

function save_method_POST(Web $w)
{
    $w->setLayout(null);

    $postdata = file_get_contents("php://input");
    header('Content-type: application/json');
    $request = json_decode($postdata);
    if (!empty($request->id)) {
        $method = FnCookbookService::getInstance($w)->getMethod($request->id);
        if (!empty($method)) {
            $method->is_deleted = $request->is_deleted;
            $method->update();
        }
    }
    $w->out(json_encode(['error' => false]));
    return;
}