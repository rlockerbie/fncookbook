<?php

function export_cookbook($w)
{
    $book = [];
    $recipes = FnCookbookService::getInstance($w)->getRecipes('');
    if (!empty($recipes)) {
        foreach ($recipes as $recipe) {
            $page = $recipe->toArray();
            $page['methods'] = FnCookbookService::getInstance($w)->loadMethodsForRecipe($recipe->id);
            $page['tags'] = [];
            $tags = TagService::getInstance($w)->getTagsByObject($recipe);
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $page['tags'][] = $tag->tag;
                }
            }
            $book[] = $page;
        }
    }
    $tagObjects = TagService::getInstance($w)->getTagsByObjectClass('FnCookBookRecipe');
    $tags = [];
    if (!empty($tagObjects)) {
        foreach ($tagObjects as $tag) {
            if (!in_array($tag->tag, $tags)) {
                $tags[] = $tag->tag;
            }
        }
    }
    $ingredients = [];
    $query = 'SELECT fn_cookbook_ingredient.title, fn_cookbook_method.recipe_id
    FROM fn_cookbook_ingredient
    LEFT JOIN fn_cookbook_method ON fn_cookbook_ingredient.method_id = fn_cookbook_method.id
    LEFT JOIN fn_cookbook_recipe ON fn_cookbook_method.recipe_id = fn_cookbook_recipe.id
    WHERE fn_cookbook_ingredient.is_deleted = 0 AND fn_cookbook_method.is_deleted = 0 AND fn_cookbook_recipe.is_deleted = 0
    ORDER BY fn_cookbook_ingredient.title ASC';
    $statement = FnCookbookService::getInstance($w)->_db->prepare($query);
    if ($statement->execute()) {
        $stopWords = [
            'natural', 'extract', ' of ', 'one', 'can', 'puree', 'ground', 'chopped', 'block'
        ];
        $ingredientData = $statement->fetchAll(PDO::FETCH_NAMED);
        foreach ($ingredientData as $ingredientRow) {
            $title = str_replace($stopWords, '', strtolower($ingredientRow['title']));
            $title = preg_replace('%\s{2,}%', ' ', $title);
            $title = str_replace(' ', '-', trim($title));
            if (empty($title)) {
                continue;
            }
            if (empty($ingredients[$title])) {
                $ingredients[$title] = [
                    'title' => $ingredientRow['title'],
                    'recipes' => []
                ];
            }
            if (!in_array($ingredientRow['recipe_id'], $ingredients[$title]['recipes'])) {
                $ingredients[$title]['recipes'][] = $ingredientRow['recipe_id'];
            }
        }
    }
    $cookbook = ['recipes' => $book, 'tags' => $tags, 'ingredients' => $ingredients];
    file_put_contents('cookbook.json', json_encode($cookbook, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
}

function fncookbook_core_dbobject_after_insert_FnCookBookRecipe(Web $w, $recipe)
{
    export_cookbook($w);
}

function fncookbook_core_dbobject_after_update_FnCookBookRecipe(Web $w, $recipe)
{
    export_cookbook($w);
}
