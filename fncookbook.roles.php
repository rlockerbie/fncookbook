<?php
function role_cookbook_admin_allowed($w, $path) {
	return startsWith($path, "cookbook");
}

function role_cookbook_view_allowed($w, $path) {
	$actions = "/cookbook\/(index";
    $actions .= "|view";
    $actions .= ")/";
    return preg_match($actions, $path);
}