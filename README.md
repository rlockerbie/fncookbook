# FNCookbook

Frill Neck Cookbook - Not for cooking lizards!

## Installing

* You will need to build the app with npm
* Run npm i && npm run build inside assets/cookbook/src

## Usage

There are two separate sections, a viewer and admin section. The viewew will be displayed by default, the admin section is accessed simply by visiting /fncookbook/admin.

From the admin section you can add or edit existing recipes. Recipes support tags so you can categorise your recipes. These tags will allow you to filter from the viewer.