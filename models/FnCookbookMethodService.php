<?php
/**
 * FnCookbookMethodService class
 * 
 * @author Robert Lockerbie, June 2018
 */
 
class FnCookbookMethodService extends DbService {
	
	/** 
	 * @return an array of all undeleted FnCookbookMethod records from the database
	 */
	function getAllData($where = array("is_deleted" => 0)) {
		return $this->getObjects("FnCookbookMethod",$where);
	}
	
	
	
	/**
	 * @param integer $id
	 * @return an ExampleData object for this id
	 */
	function getDataForId($id) {
		return $this->getObject("FnCookbookMethod",$id);
	}
	
	/**
	 * Generate a list of menu entries which will go into a drop down
	 * under the module name.
	 * 
	 * @param Web $w
	 * @param string $title (not in use)
	 * @param string $nav (not  in use)
	 * @return array of menu entries
	 */
	public function navigation(Web $w, $title = null, $nav = null) {
		$nav = array();
		if ($w->Auth->loggedIn()) {
			$w->menuLink("/ncookbook/index", "Home", $nav);
		}
		return $nav;
	}	
}