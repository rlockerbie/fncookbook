<?php
/**
 * FnCookbookRecipeService class
 * 
 * @author Robert Lockerbie, June 2018
 */

class FnCookbookRecipeService extends DbService {
	
	function loadMethodsForRecipe($id) {
		$methodsData = $this->getObjects("FnCookbookMethod", ['is_deleted' => 0, 'recipe_id' => $id]);
		$methods = [];
		$parser = new Parsedown();
		if(!empty($methodsData)) {
			foreach($methodsData as $methodRow) {
				$methods[$methodRow->id] = [
					'id' => $methodRow->id,
					'method_parsed' => $parser->parse($methodRow->method),
					'method' => $methodRow->method,
					'ingredients' => [],
				];
				$ingredientData = $this->getObjects('FnCookbookIngredient', ['is_deleted' => 0, 'method_id' => $methodRow->id]);
				if(!empty($ingredientData)) {
					foreach($ingredientData as $ingredientRow) {
						$methods[$methodRow->id]['ingredients'][$ingredientRow->id] = [
							'id' => $ingredientRow->id,
							'title' => $ingredientRow->title,
							'quantity' => $ingredientRow->quantity,
							'unit' => $ingredientRow->unit,
						];
					}
				}
			}
		}
		return $methods;
	}
	
	/** 
	 * @return an array of all undeleted FnCookbookRecipe records from the database
	 */
	function getAllData() {
		return $this->getObjects("FnCookbookRecipe",array("is_deleted" => 0));
	}
	
	/**
	 * @param integer $id
	 * @return an ExampleData object for this id
	 */
	function getDataForId($id) {
		return $this->getObject("FnCookbookRecipe",$id);
	}
	
	/**
	 * Generate a list of menu entries which will go into a drop down
	 * under the module name.
	 * 
	 * @param Web $w
	 * @param string $title (not in use)
	 * @param string $nav (not  in use)
	 * @return array of menu entries
	 */
	public function navigation(Web $w, $title = null, $nav = null) {
		$nav = array();
		if ($w->Auth->loggedIn()) {
			$w->menuLink("/ncookbook/index", "Home", $nav);
		}
		return $nav;
	}	
}