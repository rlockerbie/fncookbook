<?php
/**
 * FnCookbookMethod class
 * 
 * @author Robert Lockerbie, June 2018
 */
class FnCookbookMethod extends DbObject {
	
	// object properties
	
	public $recipe_id;
	public $method;
	public $title;
	public $is_deleted;
	public $dt_created;
	public $dt_modified;
	public $modifier_id;
	public $creator_id;

	
	// this makes it searchable
	
	public $_searchable;
	
	// functions for how to behave when displayed in search results
	
	public function printSearchTitle() {
		return $this->method;
	}
	
	public function printSearchListing() {
		return $this->method;
	}
	
	public function printSearchUrl() {
		return "/ncookbook/show/".$this->id;
	}		
	
	// functions for implementing access restrictions, these are optional

	public function canList(User $user) {
		return $user !== null && $user->hasAnyRole(array("fn_cookbook_admin"));
	}
	
	public function canView(User $user) {
		return $user !== null && $user->hasAnyRole(array("fn_cookbook_admin"));
	}
	
	public function canEdit(User $user) {
		return $user !== null && $user->hasAnyRole(array("fn_cookbook_admin"));
	}
	
	public function canDelete(User $user) {
		return $user !== null && $user->hasAnyRole(array("fn_cookbook_admin"));
	}	
	
	// functions for how to display inside a dropdown, these are optional
	
	public function getSelectOptionTitle() {
		return $this->method;
	}
	
	public function getSelectOptionValue() {
		return $this->id;
	}
	
	// override this function to add stuff to the search index
	// DO NOT CALL $this->getIndexContent() within this function
	// or you will create an endless loop which will destroy the universe!	

	function addToIndex() {
		return null;
	}	
	
	// you could override these functions, but only if you must, 
	// otherwise just delete them from this class
	
	public function update($force_nullvalues = false, $force_validation = false ) {
		parent::update($force_nullvalues, $force_validation);
	}

	public function insert($force_validation = false ) {
		parent::insert($force_validation);
	}
	
	public function delete($force = false ) {
		parent::delete($force);
	}
	
}