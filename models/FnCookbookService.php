<?php

/**
 * FnCookbookService class
 * 
 * @author Robert Lockerbie, August 2019
 */

use \Erusev\Parsedown\Parsedown;

class FnCookbookService extends DbService
{

	function loadMethodsForRecipe($id)
	{
		$methodsData = $this->getObjects("FnCookbookMethod", ['is_deleted' => 0, 'recipe_id' => $id]);
		$methods = [];
		$parser = new Parsedown();
		if (!empty($methodsData)) {
			foreach ($methodsData as $methodRow) {
				$methodText = $methodRow->method;
				$methods[$methodRow->id] = [
					'id' => $methodRow->id,
					'title' => $methodRow->title,
					'ingredients' => []
				];
				$ingredientData = $this->getObjects('FnCookbookIngredient', ['is_deleted' => 0, 'method_id' => $methodRow->id]);
				if (!empty($ingredientData)) {
					foreach ($ingredientData as $ingredientRow) {
						$methods[$methodRow->id]['ingredients'][] = [
							'id' => $ingredientRow->id,
							'title' => $ingredientRow->title,
							'quantity' => $ingredientRow->quantity,
							'unit' => $ingredientRow->unit,
						];
					}
				}
				$methods[$methodRow->id]['method_parsed'] = $parser->toHtml($methodText);
				$methods[$methodRow->id]['method'] = html_entity_decode($methodText);
			}
		}
		return $methods;
	}

	/** 
	 * @return an array of all undeleted FnCookbookRecipe records from the database
	 */
	function getRecipes($term)
	{
		$where = ['is_deleted' => 0];
		if (!empty($term)) {
			$where['title LIKE ?'] = "%$term%";
		}
		return $this->getObjects("FnCookbookRecipe", $where, false, true, 'title ASC');
	}

	/** 
	 * @return an array of all undeleted FnCookbookRecipe records from the database
	 */
	function getRecipesByTags($term, $tags)
	{
		$recipes = $this->getRecipes($term);
		$filteredRecipes = [];
		foreach ($recipes as $recipe) {
			$rTags = TagService::getInstance($this->w)->getTagsByObject($recipe);
			$hasTag = false;
			foreach ($rTags as $tag) {
				if (in_array($tag->id, $tags)) {
					$hasTag = true;
				}
			}
			if ($hasTag) {
				$filteredRecipes[] = $recipe;
			}
		}
		return $filteredRecipes;
	}

	/**
	 * @param integer $id
	 * @return an FnCookbookMethod object for this id
	 */
	function getMethod($id)
	{
		return $this->getObject("FnCookbookMethod", $id);
	}

	/**
	 * @param integer $id
	 * @return an FnCookbookIngredient object for this id
	 */
	function getIngredient($id)
	{
		return $this->getObject("FnCookbookIngredient", $id);
	}

	/**
	 * @param integer $id
	 * @return an FnCookbookRecipe object for this id
	 */
	function getRecipe($id)
	{
		return $this->getObject("FnCookbookRecipe", $id);
	}
}
