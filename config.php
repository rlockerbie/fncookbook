<?php
Config::set('fncookbook', array(
    'active' => true,
    'path' => 'modules',
    'topmenu' => true,
    'search' => array(
        "FnCookbook" => "FnCookbookRecipe",
    ),
    'frontend' => true,
    'domain_name' => 'view.fncookbook.recipes',
    'widgets' => array(),
    'hooks' => array(
        'core_dbobject'
    ),
    'processors' => array(),
    'dependencies' => [
        'voku/simple_html_dom' => '~4.8',
        'erusev/parsedown' => '~2.0@dev'
    ]
));
