<?php
/**
 * 
 * Url:
 * 
 * //ncookbook/view/{id}
 * 
 * @param Web $w
 */
function mobile_view_GET(Web $w) {
	// parse the url into parameters
	$p = $w->pathMatch("id");
	$w->setLayout('mobile-layout');
	$w->enqueueStyle(['name' => 'fn_cookbook.css', 'uri' => '/modules//ncookbook/assets/css/fn_cookbook.css', 'weight' => 800]);
	if(empty($p['id'])) {
		$recipes = FnCookbookRecipeService::getInstance($w)->getAllData();
		$w->ctx('recipes', $recipes);
		$w->templateOut('mobile_list');
		return;
	}
	// create either a view or existing object
	if (isset($p['id'])) {
		$data = FnCookbookRecipeService::getInstance($w)->getDataForId($p['id']);
		$methods = FnCookbookRecipeService::getInstance($w)->loadMethodsForRecipe($p['id']);
	} else {
		$w->out('<p>Invalid ID</p>');
		return;
	}
	$w->ctx('id', $data->id);
	$w->ctx('title', $data->title);
	$parser = new Parsedown();
	$w->ctx('description', $parser->parse($data->description));
	$w->ctx('notes', $parser->parse($data->notes));
	$w->ctx('methods', $methods);
}
