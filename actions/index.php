<?php
/**
 * @param Web $w
 */
function index_GET(Web $w)
{
	CmfiveStyleComponentRegister::registerComponent('vendors', new CmfiveStyleComponent('/modules/fncookbook/assets/cookbook/dist/css/chunk-vendors.css'));
    CmfiveStyleComponentRegister::registerComponent('worker', new CmfiveStyleComponent('/modules/fncookbook/assets/cookbook/dist/css/view.css'));
    
    CmfiveScriptComponentRegister::registerComponent('vendors', new CmfiveScriptComponent('/modules/fncookbook/assets/cookbook/dist/js/chunk-vendors.js', ['defer' => '', 'async' => '']));
    CmfiveScriptComponentRegister::registerComponent('worker', new CmfiveScriptComponent('/modules/fncookbook/assets/cookbook/dist/js/view.js', ['defer' => '', 'async' => '']));
	$w->setLayout('fncookbook_layout');
}