<?php
/**
 * 
 * Url:
 * 
 * //ncookbook/edit/{id}
 * 
 * @param Web $w
 */
function edit_GET(Web $w) {
	// parse the url into parameters
	$p = $w->pathMatch("id");
	
	// create either a edit or existing object
	if (isset($p['id'])) {
		$data = FnCookbookRecipeService::getInstance($w)->getDataForId($p['id']);
	} else {
		$data = new FnCookbookRecipe($w);
	}
	
	// create the edit form
	$f = Html::form(array(
			array("Edit FnCookbook","section"),
			array("Title","text","title", $data->title),
			array("Description","text","description", $data->description),
			array("Notes","text","notes", $data->notes),
			array("Serves","text","serves", $data->serves),

	),$w->localUrl("//ncookbook/edit/".$p['id']),"POST"," Save ");
	
	// circumvent the template and print straight into the layout
	$w->out($f);
}

/**
 * Receive post data from FnCookbookRecipe edit form.
 * 
 * Url:
 * 
 * //ncookbook/edit/{id}
 * 
 * @param Web $w
 */
function edit_POST(Web $w) {
	$p = $w->pathMatch("id");
	if (isset($p['id'])) {
		$data = FnCookbookRecipeService::getInstance($w)->getDataForId($p['id']);
	} else {
		$data = new FnCookbookRecipe($w);
	}
	
	$data->fill($_POST);
	// fill in validation step!
	
	$data->insertOrUpdate();
	
	// go back to the list view
	$w->msg("FnCookbookRecipe updated","/ncookbook/index");
}