<?php

function admin_GET(Web $w)
{
    CmfiveStyleComponentRegister::registerComponent('vendors', new CmfiveStyleComponent('/modules/fncookbook/assets/cookbook/dist/css/chunk-vendors.css'));
    CmfiveStyleComponentRegister::registerComponent('worker', new CmfiveStyleComponent('/modules/fncookbook/assets/cookbook/dist/css/admin.css'));
    
    CmfiveScriptComponentRegister::registerComponent('vendors', new CmfiveScriptComponent('/modules/fncookbook/assets/cookbook/dist/js/chunk-vendors.js', ['defer' => '', 'async' => '']));
    CmfiveScriptComponentRegister::registerComponent('worker', new CmfiveScriptComponent('/modules/fncookbook/assets/cookbook/dist/js/admin.js', ['defer' => '', 'async' => '']));
    $w->setLayout('fncookbook_layout');
}