<?php
/**
 * 
 * Url:
 * 
 * //ncookbook/view/{id}
 * 
 * @param Web $w
 */
function view_GET(Web $w) {
	// parse the url into parameters
	$p = $w->pathMatch("id");
	$w->enqueueStyle(['name' => 'fn_cookbook.css', 'uri' => '/modules//ncookbook/assets/css/fn_cookbook.css', 'weight' => 800]);
	// create either a view or existing object
	if (isset($p['id'])) {
		$data = FnCookbookRecipeService::getInstance($w)->getDataForId($p['id']);
		$methods = FnCookbookRecipeService::getInstance($w)->loadMethodsForRecipe($p['id']);
	} else {
		$w->out('<p>Invalid ID</p>');
		return;
	}
	$w->ctx('id', $data->id);
	$w->ctx('title', $data->title);
	$parser = new Parsedown();
	$w->ctx('description', $parser->parse($data->description));
	$w->ctx('notes', $parser->parse($data->notes));
	$w->ctx('methods', $methods);
}

/**
 * Receive post data from FnCookbookRecipe view form.
 * 
 * Url:
 * 
 * //ncookbook/view/{id}
 * 
 * @param Web $w
 */
function view_POST(Web $w) {
	$p = $w->pathMatch("id");
	if (isset($p['id'])) {
		$data = FnCookbookRecipeService::getInstance($w)->getDataForId($p['id']);
	} else {
		$data = new FnCookbookRecipe($w);
	}
	
	$data->fill($_POST);
	// fill in validation step!
	
	$data->insertOrUpdate();
	
	// go back to the list view
	$w->msg("FnCookbookRecipe updated","/ncookbook/index");
}