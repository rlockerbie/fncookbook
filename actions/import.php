<?php

use voku\helper\HtmlDomParser;

function import_GET(Web $w)
{
    $f = Html::form(array(
        array("Import Recipe", "section"),
        array("URL", "text", "url"),

    ), '/fncookbook/import');

    // circumvent the template and print straight into the layout
    $w->out($f);
}

function import_POST(Web $w)
{
    $dom = HtmlDomParser::file_get_html($_POST['url']);
    $recipe = $dom->findMulti('.wprm-recipe-container');

    $recipeObj = new FnCookbookRecipe($w);
    $recipeObj->title = $recipe->find('h2', 0)->text;

    $time = $recipe->find('.wprm-recipe-time');
    $recipeObj->time_to_cook = $time[(count($time) - 1)]->text;
    $recipeObj->description = $recipe->find('.wprm-recipe-summary', 0)->text;
    $recipeObj->notes = $recipe->find('.wprm-recipe-notes', 0)->innerHtml;
    $recipeObj->insert();

    $servings = $dom->find('.wprm-recipe-servings', 0);
    if (!empty($servings)) {
        $recipeObj->serves = $servings->text;
    }

    $ingredientGroups = $dom->findMulti('.wprm-recipe-ingredient-group');
    $methods = $dom->find('.wprm-recipe-instruction-group');
    $i = 0;
    foreach ($ingredientGroups as $ingredientGroup) {
        $method = $methods[$i];
        $methodObject = new FnCookbookMethod($w);
        $methodObject->recipe_id = $recipeObj->id;
        $methodObject->title = $method->find('.wprm-recipe-group-name', 0)->text;
        if (empty($methodObject->title)) {
            $methodObject->title = 'Instructions';
        }
        $steps = $method->find('.wprm-recipe-instructions .wprm-recipe-instruction .wprm-recipe-instruction-text');
        $methodMD = '';
        $s = 1;
        foreach ($steps as $step) {
            $methodMD .= "$s. " . $step->text . "\n";
            $s++;
        }
        $methodObject->method = $methodMD;
        $methodObject->insert();
        $ingredients = $ingredientGroup->find('.wprm-recipe-ingredients .wprm-recipe-ingredient');
        foreach ($ingredients as $ingredient) {
            $ingredientObject = new FnCookbookIngredient($w);
            $ingredientObject->method_id = $methodObject->id;
            $ingredientObject->title = $ingredient->find('.wprm-recipe-ingredient-name', 0)->text;
            $ingredientObject->quantity = $ingredient->find('.wprm-recipe-ingredient-amount', 0)->text;
            $ingredientObject->unit = $ingredient->find('.wprm-recipe-ingredient-unit', 0)->text;
            $ingredientObject->insert();
        }

        $i++;
    }
    $w->msg("FnCookbookRecipe added", "fncookbook/import");
}
