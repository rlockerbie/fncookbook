// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    HelloWorld: typeof import('./src/components/HelloWorld.vue')['default']
    IconCommunity: typeof import('./src/components/icons/IconCommunity.vue')['default']
    IconDocumentation: typeof import('./src/components/icons/IconDocumentation.vue')['default']
    IconEcosystem: typeof import('./src/components/icons/IconEcosystem.vue')['default']
    IconSupport: typeof import('./src/components/icons/IconSupport.vue')['default']
    IconTooling: typeof import('./src/components/icons/IconTooling.vue')['default']
    NCard: typeof import('naive-ui')['NCard']
    NCollapse: typeof import('naive-ui')['NCollapse']
    NCollapseItem: typeof import('naive-ui')['NCollapseItem']
    NGi: typeof import('naive-ui')['NGi']
    NGrid: typeof import('naive-ui')['NGrid']
    NIcon: typeof import('naive-ui')['NIcon']
    NLayout: typeof import('naive-ui')['NLayout']
    NLayoutContent: typeof import('naive-ui')['NLayoutContent']
    NLayoutHeader: typeof import('naive-ui')['NLayoutHeader']
    NLayoutSider: typeof import('naive-ui')['NLayoutSider']
    NPageHeader: typeof import('naive-ui')['NPageHeader']
    NSpace: typeof import('naive-ui')['NSpace']
    NTag: typeof import('naive-ui')['NTag']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    TheWelcome: typeof import('./src/components/TheWelcome.vue')['default']
    WelcomeItem: typeof import('./src/components/WelcomeItem.vue')['default']
  }
}
