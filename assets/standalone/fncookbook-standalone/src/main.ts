import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import { createPinia } from "pinia";

import "./assets/main.css";

const instance = axios.create({});

const app = createApp(App);

const pinia = createPinia();
app.use(pinia);
app.provide("$http", instance);
app.use(router);

app.mount("#app");
