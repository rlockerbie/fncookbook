import { createRouter, createWebHistory } from "vue-router";
import CookbookView from "../views/CookbookView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: CookbookView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/recipe/:id",
      name: "recipe",
      component: () => import("../views/RecipeView.vue"),
      props: (route) => ({
        id_int: Number(route.params.id),
      }),
    },
  ],
});

export default router;
