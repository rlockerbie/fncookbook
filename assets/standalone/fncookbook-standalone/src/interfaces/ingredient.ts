export declare interface Ingredient {
  title: string;
  recipes: Array<number>;
}
