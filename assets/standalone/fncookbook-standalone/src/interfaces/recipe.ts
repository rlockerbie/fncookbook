declare interface Ingredient {
  id: number;
  title: string;
  quantity: number;
  unit: string;
}

declare interface Method {
  id: number;
  title: string;
  ingredients: Ingredient[];
  method_parsed: string;
  method: string;
}

export interface Recipe {
  id?: number;
  title?: string;
  description?: string;
  notes?: string;
  serves?: number;
  time_to_cook?: string;
  methods?: Method[];
  tags?: Array<string>;
}
