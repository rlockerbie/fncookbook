import { defineStore } from "pinia";
import type { Recipe } from "../interfaces/recipe";
import type { Ingredient } from "../interfaces/ingredient";

export const useCookbook = defineStore("counter", {
  state: () => {
    return {
      count: 0 as number,
      recipes: [] as Recipe[],
      tags: [] as Array<string>,
      ingredients: [] as Ingredient[],
      selectedTags: [] as Array<string>,
      selectedIngredients: [] as Array<Ingredient>,
    };
  },
  getters: {
    getRecipes: (state) => {
      return () => {
        let recipes = state.recipes;
        if (state.selectedTags.length !== 0) {
          recipes = recipes.filter((recipe: Recipe) => {
            if (state.selectedTags.length === 0) {
              return true;
            }
            if (recipe.tags !== undefined && recipe.tags.length === 0) {
              return false;
            }
            return (
              recipe.tags !== undefined &&
              recipe.tags.findIndex((tag: string) => {
                return (
                  state.selectedTags.findIndex((t: string) => t === tag) !== -1
                );
              }) !== -1
            );
          });
        }
        if (state.selectedIngredients.length !== 0) {
          let ids = [] as Array<number>;
          state.selectedIngredients.forEach((ingredient: Ingredient) => {
            ids = ids.concat(ingredient.recipes);
          });
          recipes = recipes.filter((recipe: Recipe) => {
            const id = recipe.id as number;
            return ids.indexOf(id) !== -1;
          });
        }
        return recipes;
      };
    },
    getRecipesByTags: (state) => {
      return () =>
        state.recipes.filter((recipe: Recipe) => {
          if (state.selectedTags.length === 0) {
            return true;
          }
          if (recipe.tags !== undefined && recipe.tags.length === 0) {
            return false;
          }
          return (
            recipe.tags !== undefined &&
            recipe.tags.findIndex((tag: string) => {
              return (
                state.selectedTags.findIndex((t: string) => t === tag) !== -1
              );
            }) !== -1
          );
        });
    },
    getRecipeById: (state) => {
      return (recipeId: number) =>
        state.recipes.find((recipe) => {
          return recipe.id === recipeId;
        });
    },
  },
});
