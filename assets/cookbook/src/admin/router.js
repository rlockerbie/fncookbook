import Vue from 'vue'
import Router from 'vue-router'

import Page from '@/components/Page.vue'
import Admin from '@/components/Admin.vue'
import Recipe from '@/components/Recipe.vue'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'book',
			component: Admin
		},
		{
			path: '/recipe/:recipe_id',
			name: 'recipe',
			component: Page
		},
		{
			path: '/edit/:recipe_id?',
			name: 'edit',
			component: Recipe
		}
	]
})
