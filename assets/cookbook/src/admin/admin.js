import Vue from 'vue'
import axios from 'axios'
import App from './Admin.vue'
import vuetify from '@/plugins/vuetify';
import router from './router'
import Vue2Editor from "vue2-editor";

Vue.config.productionTip = false
const instance = axios.create({
	baseURL: process.env.NODE_ENV != 'production' ? 'https://fnframeworks.mooo.com' : ''
});
Vue.prototype.$http = instance;
new Vue({
  vuetify,
  router,
  Vue2Editor,
  render: h => h(App)
}).$mount('#app')
