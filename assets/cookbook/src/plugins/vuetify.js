import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        "primary": "#ff5722",
        "secondary": "#9c27b0",
        "accent": "#4dd0e1",
        "error": "#FF5252",
        "info": "#00796b",
        "success": "#4CAF50",
        "warning": "#FB8C00"
      }
    }
  }
});
