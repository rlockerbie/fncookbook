import Vue from 'vue'
import axios from 'axios'
import App from './View.vue'
import vuetify from '@/plugins/vuetify';
import router from './router'

Vue.config.productionTip = false
const instance = axios.create({
	baseURL: process.env.NODE_ENV != 'production' ? 'https://fnframeworks.mooo.com' : ''
});
Vue.prototype.$http = instance;
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
