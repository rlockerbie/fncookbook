import Vue from 'vue'
import Router from 'vue-router'

import Page from '@/components/Page.vue'
import Book from '@/components/Book.vue'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'book',
			component: Book
		},
		{
			path: '/recipe/:recipe_id',
			name: 'recipe',
			component: Page
		},
	]
})
