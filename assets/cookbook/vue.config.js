// vue.config.js
const path = require('path')

module.exports = {
  filenameHashing: false,
  publicPath: '/modules/fncookbook/assets/app/dist',
	pages: {
		admin: {
			entry: './src/admin/admin.js',
			title: "Cookbook Admin",
			chunks: ['chunk-vendors', 'chunk-common', 'admin'],
		},
		view: {
			entry: './src/view/view.js',
			title: "Cookbook",
			chunks: ['chunk-vendors', 'chunk-common', 'view'],
		}
	},
}
